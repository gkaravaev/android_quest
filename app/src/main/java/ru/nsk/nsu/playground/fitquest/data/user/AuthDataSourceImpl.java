package ru.nsk.nsu.playground.fitquest.data.user;

import io.reactivex.rxjava3.core.Observable;
import ru.nsk.nsu.playground.fitquest.model.common.ResponseResult;
import ru.nsk.nsu.playground.fitquest.model.user.Credentials;
import ru.nsk.nsu.playground.fitquest.model.user.LoginResult;
import ru.nsk.nsu.playground.fitquest.model.user.RegistrationRequest;

import javax.inject.Inject;

public class AuthDataSourceImpl implements IAuthDataSource {

    private final AuthenticationService authService;

    @Inject
    public AuthDataSourceImpl(AuthenticationService authService) {
        this.authService = authService;
    }

    @Override
    public Observable<LoginResult> login(Credentials credentials) {
        return authService.login(credentials);
    }

    @Override
    public Observable<ResponseResult> registration(RegistrationRequest request) {
        return authService.register(request);
    }
}
