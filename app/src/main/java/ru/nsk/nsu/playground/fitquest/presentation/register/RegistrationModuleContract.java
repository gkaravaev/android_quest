package ru.nsk.nsu.playground.fitquest.presentation.register;

import android.app.Activity;
import ru.nsk.nsu.playground.fitquest.model.user.Credentials;
import ru.nsk.nsu.playground.fitquest.model.user.RegistrationRequest;
import ru.nsk.nsu.playground.fitquest.presentation.common.IBasePresenter;
import ru.nsk.nsu.playground.fitquest.presentation.common.IBaseView;

public interface RegistrationModuleContract {

    interface View extends IBaseView<RegistrationModuleContract.Presenter> {
        void onRegistrationSuccess();
        void onRegistrationFail(String message);
    }

    interface Presenter extends IBasePresenter {
        void register(RegistrationRequest request);
        void onDestroy();
    }

    interface Router {
        void setActivity(final Activity activity);
        void goToLogin();
    }
}
