package ru.nsk.nsu.playground.fitquest.presentation.common;

public final class PresentationLayerError {

    private final String title;

    public PresentationLayerError(final String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }
}
