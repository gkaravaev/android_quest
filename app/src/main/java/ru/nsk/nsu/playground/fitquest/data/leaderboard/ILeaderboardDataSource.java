package ru.nsk.nsu.playground.fitquest.data.leaderboard;

import io.reactivex.rxjava3.core.Observable;
import ru.nsk.nsu.playground.fitquest.model.common.ResponseResult;
import ru.nsk.nsu.playground.fitquest.model.leaderboard.LeaderboardItem;
import ru.nsk.nsu.playground.fitquest.model.stage.Answer;
import ru.nsk.nsu.playground.fitquest.model.stage.AnswerResult;
import ru.nsk.nsu.playground.fitquest.model.stage.Stage;
import ru.nsk.nsu.playground.fitquest.model.stage.TaskCreateRequest;

public interface ILeaderboardDataSource {
    Observable<LeaderboardItem[]> getLeaderboard();
}
