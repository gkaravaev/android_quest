package ru.nsk.nsu.playground.fitquest.presentation.stage.tasklist;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import ru.nsk.nsu.playground.fitquest.R;
import ru.nsk.nsu.playground.fitquest.model.stage.Task;

import java.util.List;

public final class TasksListAdapter extends RecyclerView.Adapter<TasksListItemViewHolder> {

    private final TasksListModuleContract.Router router;
    private List<Task> items;

    public TasksListAdapter(final TasksListModuleContract.Router router, List<Task> list) {
        this.router = router;
        this.items = list;
    }

    void replaceData(final List<Task> items) {
        if (null != items) {
            this.items = items;
        }
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public TasksListItemViewHolder onCreateViewHolder(@NonNull final ViewGroup parent, final int viewType) {
        final Context context = parent.getContext();

        final View view = LayoutInflater
                .from(context)
                .inflate(R.layout.item_list_task, parent, false);

        return new TasksListItemViewHolder(view, router);
    }

    @Override
    public void onBindViewHolder(@NonNull final TasksListItemViewHolder holder, final int position) {
        final Task item = items.get(position);
        holder.onBind(item);
    }

    @Override
    public int getItemCount() {
        return (null != items) ? items.size() : 0;
    }
}
