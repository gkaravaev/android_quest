package ru.nsk.nsu.playground.fitquest.presentation.stage.tasklist;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import ru.nsk.nsu.playground.fitquest.R;
import ru.nsk.nsu.playground.fitquest.model.stage.Task;

public final class TasksListItemViewHolder extends RecyclerView.ViewHolder {

    private static final String LOG_TAG = TasksListItemViewHolder.class.getCanonicalName();

    private TextView titleTextView;
    private TextView completion;
    private TextView pointsText;

    private final TasksListModuleContract.Router router;
    private Task item;

    public TasksListItemViewHolder(@NonNull final View itemView, final TasksListModuleContract.Router router) {
        super(itemView);

        this.router = router;

        titleTextView = itemView.findViewById(R.id.task_title_text_view);
        completion = itemView.findViewById(R.id.item_task_completion);
        pointsText = itemView.findViewById(R.id.task_points);

        itemView.setOnClickListener(onItemClickListener());
    }

    void onBind(final Task item) {
        this.item = item;
        titleTextView.setText(item.getName());
        pointsText.setText(String.format("%d очков", item.getPoints()));
        if (item.getIsCompleted()) {
            completion.setText("Выполнено");
            completion.setTextColor(Color.parseColor("#73a839"));
        } else {
            completion.setText("Не выполнено");
            completion.setTextColor(Color.parseColor("#868e96"));
        }
    }

    private View.OnClickListener onItemClickListener() {
        return view -> {
            if (null == item) {
                Log.e(LOG_TAG, "Task item is null in ViewHolder.");
                return;
            }
            router.goToTheDetailsScreen(item);
        };
    }
}