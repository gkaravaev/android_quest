package ru.nsk.nsu.playground.fitquest.presentation.leaderboard;

import android.app.Activity;
import android.util.Log;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import retrofit2.HttpException;
import ru.nsk.nsu.playground.fitquest.data.leaderboard.ILeaderboardDataSource;
import ru.nsk.nsu.playground.fitquest.model.leaderboard.LeaderboardItem;
import ru.nsk.nsu.playground.fitquest.presentation.shop.ShopActivity;
import ru.nsk.nsu.playground.fitquest.presentation.stage.tasklist.TasksListActivity;
import ru.nsk.nsu.playground.fitquest.utils.UtilsFunctions;

import java.util.Arrays;
import java.util.List;

public final class LeaderboardListPresenter implements LeaderboardModuleContract.Presenter, LeaderboardModuleContract.Router {

    private static final String LOG_TAG = LeaderboardListPresenter.class.getCanonicalName();

    private final LeaderboardModuleContract.View view;

    ILeaderboardDataSource leaderboardRepository;

    private Activity activity;
    private io.reactivex.rxjava3.disposables.Disposable toDestroy;

    public LeaderboardListPresenter(LeaderboardModuleContract.View view, ILeaderboardDataSource dataSource) {
        this.view = view;
        this.leaderboardRepository = dataSource;
        view.linkPresenter(this);
    }

    @Override
    public void loadLeaders() {
        Log.i(LOG_TAG, "Loading leaderboard.");
        toDestroy = leaderboardRepository
                .getLeaderboard()
                .subscribeOn(io.reactivex.rxjava3.schedulers.Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        stSuccess -> {
                            List<LeaderboardItem> leaderboardItems = Arrays.asList(stSuccess);
                            Log.d(LOG_TAG, "Successfully obtained " + leaderboardItems.size() + " leaders.");
                            view.showLeaders(leaderboardItems);
                        },
                        stFail -> {
                            String errorMessage = stFail.getLocalizedMessage();
                            if (stFail instanceof HttpException) {
                                errorMessage = UtilsFunctions.httpExceptionToString((HttpException) stFail);
                            }
                            Log.e(LOG_TAG, errorMessage);
                        }
                );
    }

    @Override
    public void onDestroy() {
        if (toDestroy != null && !toDestroy.isDisposed()){
            toDestroy.dispose();
        }
    }

    @Override
    public void setActivity(final Activity activity) {
        this.activity = activity;
    }

    @Override
    public void goToTasksScreen() {
        UtilsFunctions.travelToActivity(this.activity, TasksListActivity.class);
    }

    @Override
    public void goToShopScreen() {
        UtilsFunctions.travelToActivity(this.activity, ShopActivity.class);
    }
}
