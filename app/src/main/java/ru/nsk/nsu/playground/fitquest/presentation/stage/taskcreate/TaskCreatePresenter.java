package ru.nsk.nsu.playground.fitquest.presentation.stage.taskcreate;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import retrofit2.HttpException;
import ru.nsk.nsu.playground.fitquest.data.stage.IStageDataSource;
import ru.nsk.nsu.playground.fitquest.model.stage.TaskCreateRequest;
import io.reactivex.rxjava3.disposables.Disposable;
import ru.nsk.nsu.playground.fitquest.utils.UtilsFunctions;

public class TaskCreatePresenter implements TaskCreateModuleContract.Presenter {

    private IStageDataSource stageDataSource;

    private Disposable taskCreateDisposable;

    private TaskCreateModuleContract.View view;

    public TaskCreatePresenter(TaskCreateModuleContract.View view, IStageDataSource stageRepository) {
        this.stageDataSource = stageRepository;
        this.view = view;
    }

    @Override
    public void createTask(TaskCreateRequest request) {
        taskCreateDisposable = stageDataSource
                .createTask(request)
                .subscribeOn(io.reactivex.rxjava3.schedulers.Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(success -> {
                    view.onTaskCreate();
                }, stFail -> {
                    String errorMessage = stFail.getLocalizedMessage();
                    if (stFail instanceof HttpException) {
                        errorMessage = UtilsFunctions.httpExceptionToString((HttpException) stFail);
                    }
                    view.onError(errorMessage);
                });
    }

    @Override
    public void onDestroy() {
        if (taskCreateDisposable != null && !taskCreateDisposable.isDisposed()) {
            taskCreateDisposable.dispose();
        }
    }
}
