package ru.nsk.nsu.playground.fitquest.presentation.leaderboard;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import ru.nsk.nsu.playground.fitquest.R;
import ru.nsk.nsu.playground.fitquest.model.leaderboard.LeaderboardItem;
import ru.nsk.nsu.playground.fitquest.model.stage.Task;

import java.util.List;

public final class LeaderboardListAdapter extends RecyclerView.Adapter<LeaderboardItemViewHolder> {

    private final LeaderboardModuleContract.Router router;
    private List<LeaderboardItem> items;

    public LeaderboardListAdapter(final LeaderboardModuleContract.Router router, List<LeaderboardItem> list) {
        this.router = router;
        this.items = list;
    }

    void replaceData(final List<LeaderboardItem> items) {
        if (null != items) {
            this.items = items;
        }
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public LeaderboardItemViewHolder onCreateViewHolder(@NonNull final ViewGroup parent, final int viewType) {
        final Context context = parent.getContext();

        final View view = LayoutInflater
                .from(context)
                .inflate(R.layout.item_list_leaderboard, parent, false);

        return new LeaderboardItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final LeaderboardItemViewHolder holder, final int position) {
        final LeaderboardItem item = items.get(position);
        holder.onBind(item, position);
    }

    @Override
    public int getItemCount() {
        return (null != items) ? items.size() : 0;
    }
}
