package ru.nsk.nsu.playground.fitquest.model.stage;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.ArrayList;

@Getter
@Setter

public class Stage {
    private Long id;

    private String name;

    private String start;

    private String end;

    private String created;

    private String updated;

    private ArrayList<Task> tasks;

    public Stage(Long id, String name, String start, String end, ArrayList<Task> tasks) {
        this.id = id;
        this.name = name;
        this.start = start;
        this.end = end;
        this.tasks = tasks;
    }
}
