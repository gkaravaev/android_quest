package ru.nsk.nsu.playground.fitquest.data.leaderboard;

import io.reactivex.rxjava3.core.Observable;
import ru.nsk.nsu.playground.fitquest.model.leaderboard.LeaderboardItem;

import javax.inject.Inject;

public class LeaderboardDataSourceImpl implements ILeaderboardDataSource {

    private final LeaderboardService leaderboardService;

    @Inject
    public LeaderboardDataSourceImpl(LeaderboardService leaderboardService) {
        this.leaderboardService = leaderboardService;
    }

    @Override
    public Observable<LeaderboardItem[]> getLeaderboard() {
        return leaderboardService.getLeaderboard();
    }
}
