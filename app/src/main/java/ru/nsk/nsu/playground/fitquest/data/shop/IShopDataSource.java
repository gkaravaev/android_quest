package ru.nsk.nsu.playground.fitquest.data.shop;

import ru.nsk.nsu.playground.fitquest.model.common.ResponseResult;
import ru.nsk.nsu.playground.fitquest.model.shop.Item;

import java.util.List;
import io.reactivex.rxjava3.core.Observable;
public interface IShopDataSource {
    Observable<List<Item>> getShop();
    Observable<ResponseResult> buyItem(String id);
}
