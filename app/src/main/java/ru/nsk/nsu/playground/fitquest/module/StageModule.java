package ru.nsk.nsu.playground.fitquest.module;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import dagger.hilt.InstallIn;
import dagger.hilt.android.components.ActivityComponent;
import retrofit2.Retrofit;
import ru.nsk.nsu.playground.fitquest.data.stage.IStageDataSource;
import ru.nsk.nsu.playground.fitquest.data.stage.StageDataSourceImpl;
import ru.nsk.nsu.playground.fitquest.data.stage.StageService;


@Module
@InstallIn(ActivityComponent.class)
public abstract class StageModule {

    @Provides
    public static StageService provideStageService(Retrofit retrofit) {
        return retrofit.create(StageService.class);
    }

    @Binds
    public abstract IStageDataSource bindStageDataSource(StageDataSourceImpl stageDataSource);

}

