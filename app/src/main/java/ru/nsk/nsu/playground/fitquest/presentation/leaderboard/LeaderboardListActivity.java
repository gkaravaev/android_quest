package ru.nsk.nsu.playground.fitquest.presentation.leaderboard;

import android.os.Bundle;
import android.util.Log;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import dagger.hilt.android.AndroidEntryPoint;
import ru.nsk.nsu.playground.fitquest.R;
import ru.nsk.nsu.playground.fitquest.data.leaderboard.ILeaderboardDataSource;
import ru.nsk.nsu.playground.fitquest.data.stage.IStageDataSource;
import ru.nsk.nsu.playground.fitquest.model.leaderboard.LeaderboardItem;
import ru.nsk.nsu.playground.fitquest.model.stage.Task;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

@AndroidEntryPoint
public final class LeaderboardListActivity extends AppCompatActivity implements LeaderboardModuleContract.View {

    private static final String LOG_TAG = LeaderboardListActivity.class.getSimpleName();

    private LeaderboardModuleContract.Presenter presenter;

    private RecyclerView recyclerView;
    private BottomNavigationView bnv;

    private LeaderboardListAdapter listAdapter;

    @Inject
    ILeaderboardDataSource leaderboardDataSource;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leaderboard);

        LeaderboardListPresenter presenter = new LeaderboardListPresenter(this, leaderboardDataSource);
        presenter.setActivity(this);

        setTitle("Доска почета");

        bnv = findViewById(R.id.lb_list_bottom_navigation);

        bnv.setOnNavigationItemSelectedListener(item -> {
            switch (item.getItemId()){
                case R.id.page_tasks:
                    presenter.goToTasksScreen();
                    return true;
                case R.id.page_shop:
                    presenter.goToShopScreen();
                    return true;
                case R.id.page_leaderboard:
                    return true;
            }
            return false;
        });

        this.listAdapter = new LeaderboardListAdapter(presenter, new ArrayList<>());

        recyclerView = findViewById(R.id.recycler_view_leaderboard);
        final RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(listAdapter);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(LOG_TAG, "onStart: ");
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(LOG_TAG, "onResume: ");
        presenter.loadLeaders();
    }

    @Override
    public void onDestroy() {
        presenter.onDestroy();
        Log.d(LOG_TAG, "onDestroy: ");
        presenter = null;
        super.onDestroy();
    }

    @Override
    public void linkPresenter(final LeaderboardModuleContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void showLeaders(final List<LeaderboardItem> items) {
        listAdapter.replaceData(items);
    }
}
