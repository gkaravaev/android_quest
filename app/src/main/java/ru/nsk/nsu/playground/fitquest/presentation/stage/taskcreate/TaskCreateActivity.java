package ru.nsk.nsu.playground.fitquest.presentation.stage.taskcreate;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import dagger.hilt.android.AndroidEntryPoint;
import ru.nsk.nsu.playground.fitquest.R;
import ru.nsk.nsu.playground.fitquest.data.stage.IStageDataSource;
import ru.nsk.nsu.playground.fitquest.model.stage.TaskCreateRequest;
import ru.nsk.nsu.playground.fitquest.presentation.login.LoginPresenter;

import javax.inject.Inject;

@AndroidEntryPoint
public class TaskCreateActivity extends AppCompatActivity implements TaskCreateModuleContract.View {

    private TaskCreateModuleContract.Presenter presenter;

    private Button createTaskButton;
    private EditText title;
    private EditText text;
    private EditText answer;
    private EditText points;
    private TextView statusText;

    @Inject
    IStageDataSource stageRepository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_task);

        title = findViewById(R.id.task_create_name);
        text = findViewById(R.id.task_create_text);
        answer = findViewById(R.id.task_create_answer);
        points = findViewById(R.id.task_create_points);
        createTaskButton = findViewById(R.id.submit_task_button);
        statusText = findViewById(R.id.task_create_status);

        TaskCreateModuleContract.Presenter presenter = new TaskCreatePresenter(this, stageRepository);
        this.linkPresenter(presenter);

        createTaskButton.setOnClickListener(this::onTaskCreate);
    }

    private void onTaskCreate(View view) {
        String name = title.getText().toString();
        String text = this.text.getText().toString();
        String answer = this.answer.getText().toString();
        int points = Integer.parseInt(this.points.getText().toString());
        TaskCreateRequest request = new TaskCreateRequest(name, text, answer, points);
        presenter.createTask(request);
    }

    @Override
    protected void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onTaskCreate() {
        statusText.setText("Задание создано успешно!");
        statusText.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary));
    }

    @Override
    public void onError(String error) {
        statusText.setText(error);
        statusText.setTextColor(Color.RED);
    }

    @Override
    public void linkPresenter(TaskCreateModuleContract.Presenter presenter) {
        this.presenter = presenter;
    }
}
