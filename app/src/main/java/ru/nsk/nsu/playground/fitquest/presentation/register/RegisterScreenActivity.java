package ru.nsk.nsu.playground.fitquest.presentation.register;

import android.graphics.Color;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import dagger.hilt.android.AndroidEntryPoint;
import ru.nsk.nsu.playground.fitquest.R;
import ru.nsk.nsu.playground.fitquest.data.user.IAuthDataSource;
import ru.nsk.nsu.playground.fitquest.model.user.RegistrationRequest;

import javax.inject.Inject;

@AndroidEntryPoint
public class RegisterScreenActivity extends AppCompatActivity implements RegistrationModuleContract.View {

    private EditText email;
    private EditText firstName;
    private EditText lastName;
    private EditText password;
    private EditText passwordConf;
    private TextView errorText;
    private TextView link;
    private Button regButton;
    private RegistrationModuleContract.Presenter presenter;

    @Inject
    IAuthDataSource authRepository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_screen);

        regButton = findViewById(R.id.reg_button);
        email = findViewById(R.id.reg_email);
        password = findViewById(R.id.reg_pas);
        passwordConf = findViewById(R.id.reg_pas_conf);
        errorText = findViewById(R.id.reg_status);
        firstName = findViewById(R.id.reg_name);
        lastName = findViewById(R.id.reg_surname);
        link = findViewById(R.id.reg_link);

        RegistrationModuleContract.Presenter presenter = new RegistrationPresenter(this, authRepository);
        this.linkPresenter(presenter);

        link.setOnClickListener(view -> {
            ((RegistrationModuleContract.Router) presenter).goToLogin();
        });

        regButton.setOnClickListener(view -> {
            String email = this.email.getText().toString();
            String password = this.password.getText().toString();
            String passwordConf = this.passwordConf.getText().toString();
            String firstName = this.firstName.getText().toString();
            String lastName = this.lastName.getText().toString();
            if (!password.equals(passwordConf)) {
                this.onRegistrationFail("Пароли не совпадают");
                return;
            }
            if (password.length() < 8) {
                this.onRegistrationFail("Длина пароля должна быть больше 8 символов");
                return;
            }
            presenter.register(new RegistrationRequest(email, password, firstName, lastName));
        });
    }

    @Override
    protected void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onRegistrationSuccess() {
        errorText.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary));
        errorText.setText("Регистрация успешна!");
    }

    @Override
    public void onRegistrationFail(String message) {
        errorText.setTextColor(Color.RED);
        errorText.setText(message);
    }

    @Override
    public void linkPresenter(RegistrationModuleContract.Presenter presenter) {
        this.presenter = presenter;
        ((RegistrationModuleContract.Router) presenter).setActivity(this);
    }
}
