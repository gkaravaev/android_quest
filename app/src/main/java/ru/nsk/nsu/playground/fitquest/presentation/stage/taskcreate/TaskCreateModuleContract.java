package ru.nsk.nsu.playground.fitquest.presentation.stage.taskcreate;

import ru.nsk.nsu.playground.fitquest.model.stage.TaskCreateRequest;
import ru.nsk.nsu.playground.fitquest.presentation.common.IBasePresenter;
import ru.nsk.nsu.playground.fitquest.presentation.common.IBaseView;

public interface TaskCreateModuleContract {

    interface View extends IBaseView<Presenter> {
        void onTaskCreate();
        void onError(String error);
    }

    interface Presenter extends IBasePresenter {
        void createTask(TaskCreateRequest request);
        void onDestroy();
    }
}
