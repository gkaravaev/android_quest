package ru.nsk.nsu.playground.fitquest.presentation.stage.tasklist;

import android.os.Bundle;
import android.util.Log;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import dagger.hilt.android.AndroidEntryPoint;
import ru.nsk.nsu.playground.fitquest.R;
import ru.nsk.nsu.playground.fitquest.data.stage.IStageDataSource;
import ru.nsk.nsu.playground.fitquest.model.stage.Task;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

@AndroidEntryPoint
public final class TasksListActivity extends AppCompatActivity implements TasksListModuleContract.View {

    private static final String LOG_TAG = TasksListActivity.class.getSimpleName();

    private TasksListModuleContract.Presenter presenter;

    private RecyclerView recyclerView;
    private FloatingActionButton fab;
    private BottomNavigationView bnv;

    private TasksListAdapter listAdapter;

    @Inject
    IStageDataSource stageDataSource;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tasks_list);

        TasksListPresenter presenter = new TasksListPresenter(this, stageDataSource);
        presenter.setActivity(this);

        setTitle("Задания");

        bnv = findViewById(R.id.list_bottom_navigation);

        bnv.setOnNavigationItemSelectedListener(item -> {
            switch (item.getItemId()){
                case R.id.page_tasks:
                    return true;
                case R.id.page_shop:
                    presenter.goToShop();
                    return true;
                case R.id.page_leaderboard:
                    presenter.goToLeaderboard();
                    return true;
            }
            return false;
        });

        fab = findViewById(R.id.floatingActionButton);
        fab.setOnClickListener(v -> presenter.goToTaskCreateScreen());

        this.listAdapter = new TasksListAdapter(presenter, new ArrayList<>());

        recyclerView = findViewById(R.id.recycler_view_tasks);
        final RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(listAdapter);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(LOG_TAG, "onStart: ");
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(LOG_TAG, "onResume: ");
        presenter.loadTasks();
    }

    @Override
    public void onDestroy() {
        presenter.onDestroy();
        Log.d(LOG_TAG, "onDestroy: ");
        presenter = null;
        super.onDestroy();
    }

    @Override
    public void linkPresenter(final TasksListModuleContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void showTasks(final List<Task> items) {
        listAdapter.replaceData(items);
    }

}
