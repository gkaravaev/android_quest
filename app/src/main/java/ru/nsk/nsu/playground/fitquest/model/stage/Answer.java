package ru.nsk.nsu.playground.fitquest.model.stage;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class Answer {
    private String answer;

    public Answer(String answer) {
        this.answer = answer;
    }
}
