package ru.nsk.nsu.playground.fitquest.model.stage;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AnswerResult {
    private String result;

    public AnswerResult(String result) {
        this.result = result;
    }
}
