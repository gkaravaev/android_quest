package ru.nsk.nsu.playground.fitquest;

import android.app.Application;
import dagger.hilt.android.HiltAndroidApp;

@HiltAndroidApp
public class MainApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
    }
}
