package ru.nsk.nsu.playground.fitquest.presentation.stage.taskdetails;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import dagger.hilt.android.AndroidEntryPoint;
import retrofit2.HttpException;
import ru.nsk.nsu.playground.fitquest.R;
import ru.nsk.nsu.playground.fitquest.application.Constants;
import ru.nsk.nsu.playground.fitquest.data.stage.IStageDataSource;
import ru.nsk.nsu.playground.fitquest.model.stage.Answer;
import ru.nsk.nsu.playground.fitquest.model.stage.Task;
import io.reactivex.rxjava3.disposables.Disposable;
import ru.nsk.nsu.playground.fitquest.utils.UtilsFunctions;

import javax.inject.Inject;

@AndroidEntryPoint
public class TaskDetailsActivity extends AppCompatActivity{

    @Inject
    IStageDataSource stageDataSource;

    private TextView taskText;
    private EditText answer;
    private Button answerButton;
    private TextView status;
    private TextView points;

    private String taskId;
    private Disposable disposable;

    public static Intent getIntent(final Context context, final Task task) {
        return new Intent(context, TaskDetailsActivity.class).putExtra(Constants.Extra.TASK, task);
    }

    public void onAnswerSubmit(View view) {
        String answerToSubmit = answer.getText().toString();
        Answer answer = new Answer(answerToSubmit);
        disposable = stageDataSource.answer(taskId, answer)
                .subscribe(success -> {
                    String result = success.getResult();
                    if (result.equals("CORRECT")) {
                        status.setText("Ответ верный!");
                        status.setTextColor(Color.parseColor("#73a839"));
                    } else {
                        status.setText("Ответ неверный!");
                        status.setTextColor(Color.parseColor("#c71c22"));
                    }
                }, error -> {
                    if (error instanceof HttpException){
                        String errorMessage = UtilsFunctions.httpExceptionToString((HttpException) error);
                        status.setText(errorMessage);
                        status.setTextColor(Color.parseColor("#c71c22"));
                    }
                });
    }

    public void setTaskDetails(Task task){
        taskId = task.getId().toString();
        setTitle(task.getName());
        this.taskText.setText(task.getText());
        this.taskText.setMovementMethod(new ScrollingMovementMethod());
        answerButton.setOnClickListener(this::onAnswerSubmit);
        points.setText(task.getPoints().toString() + " очков");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_details);
        taskText = findViewById(R.id.task_text);
        answer = findViewById(R.id.answer_text);
        answerButton = findViewById(R.id.answer_button);
        status = findViewById(R.id.status);
        points = findViewById(R.id.points);

        if (getIntent().hasExtra(Constants.Extra.TASK)) {
            final Task task = getIntent().getParcelableExtra(Constants.Extra.TASK);
            if (null != task) {
                setTaskDetails(task);
            }
        }
    }

    @Override
    protected void onDestroy() {
        if (disposable != null && !disposable.isDisposed()) {
            disposable.dispose();
        }
        super.onDestroy();
    }
}