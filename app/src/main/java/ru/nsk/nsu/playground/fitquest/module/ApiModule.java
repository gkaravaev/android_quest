package ru.nsk.nsu.playground.fitquest.module;

import com.google.gson.*;
import dagger.Module;
import dagger.Provides;
import dagger.hilt.InstallIn;
import dagger.hilt.android.components.ApplicationComponent;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import ru.nsk.nsu.playground.fitquest.utils.TokenInterceptor;

import javax.inject.Singleton;
import java.lang.reflect.Type;
import java.text.DateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;

@Module
@InstallIn(ApplicationComponent.class)
public class ApiModule {
    private static Retrofit retrofit;

    @Provides
    @Singleton
    public static Retrofit getRetrofit(TokenInterceptor interceptor) {
        if (retrofit == null) {
            OkHttpClient.Builder a = new OkHttpClient.Builder();
            a.addInterceptor(interceptor);
            JsonDeserializer<LocalDateTime> ldt = (json, type, jsonDeserializationContext) -> {
                Instant instant = Instant.ofEpochMilli(json.getAsJsonPrimitive().getAsLong());
                return LocalDateTime.ofInstant(instant, ZoneId.systemDefault());
            };
            Gson gson = new GsonBuilder()
                    .setDateFormat(DateFormat.FULL, DateFormat.FULL)
                    .registerTypeAdapter(LocalDateTime.class, ldt)
                    .create();
            retrofit = new Retrofit.Builder()
                    .baseUrl("http://35.228.211.178:8081")
                    .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .client(a.build())
                    .build();
        }
        return retrofit;
    }

}
