package ru.nsk.nsu.playground.fitquest.utils;

import android.app.Activity;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import org.json.JSONException;
import org.json.JSONObject;
import retrofit2.HttpException;
import ru.nsk.nsu.playground.fitquest.presentation.stage.taskcreate.TaskCreateActivity;

import java.io.IOException;

public class UtilsFunctions {

    public static String httpExceptionToString(HttpException httpError) throws IOException {
        try {
            JSONObject jObjError = new JSONObject(httpError.response().errorBody().string());
            return jObjError.getString("message");
        } catch (JSONException e) {
            return "";
        }
    }

    public static void travelToActivity(Activity thisActivity, Class<? extends AppCompatActivity> whereTo) {
        Intent intent = new Intent(thisActivity, whereTo);
        thisActivity.startActivity(intent);
    }

}
