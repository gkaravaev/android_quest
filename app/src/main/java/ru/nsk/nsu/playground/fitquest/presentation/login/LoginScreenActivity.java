package ru.nsk.nsu.playground.fitquest.presentation.login;

import android.graphics.Color;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import dagger.hilt.android.AndroidEntryPoint;
import ru.nsk.nsu.playground.fitquest.R;
import ru.nsk.nsu.playground.fitquest.data.stage.IStageDataSource;
import ru.nsk.nsu.playground.fitquest.data.user.IAuthDataSource;
import ru.nsk.nsu.playground.fitquest.model.user.Credentials;
import ru.nsk.nsu.playground.fitquest.presentation.register.RegistrationModuleContract;
import ru.nsk.nsu.playground.fitquest.utils.TokenInterceptor;

import javax.inject.Inject;

@AndroidEntryPoint
public class LoginScreenActivity extends AppCompatActivity implements LoginModuleContract.View {

    private EditText login;
    private EditText password;
    private TextView errorText;
    private TextView link;
    private Button loginButton;
    private LoginModuleContract.Presenter presenter;

    @Inject
    IAuthDataSource authRepository;

    @Inject
    IStageDataSource stageRepository;

    @Inject
    TokenInterceptor interceptor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_screen);

        loginButton = findViewById(R.id.login_button);
        login = findViewById(R.id.login);
        password = findViewById(R.id.password);
        errorText = findViewById(R.id.errorMessage);
        link = findViewById(R.id.login_link);

        loginButton.setOnClickListener(view -> {
            String login = this.login.getText().toString();
            String password = this.password.getText().toString();
            presenter.login(new Credentials(login, password));
        });

        LoginModuleContract.Presenter presenter = new LoginPresenter(this, authRepository, interceptor);
        this.linkPresenter(presenter);

        link.setOnClickListener(view -> {
            ((LoginModuleContract.Router) presenter).goToTheRegistration();
        });
    }

    @Override
    protected void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onLoginSuccess() {
        ((LoginModuleContract.Router)presenter).goToTheTasks();
    }

    @Override
    public void onLoginFail(String message) {
        errorText.setTextColor(Color.RED);
        errorText.setText(message);
    }

    @Override
    public void linkPresenter(LoginModuleContract.Presenter presenter) {
        this.presenter = presenter;
        ((LoginModuleContract.Router)presenter).setActivity(this);
    }
}
