package ru.nsk.nsu.playground.fitquest.model.shop;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Item {
    private Long id;

    private String name;

    private String description;

    private Integer inStock;

    private Integer price;

    private String imageUrl;

    public Item(Item another) {
        this.id = another.id;
        this.name = another.name;
        this.description = another.description;
        this.inStock = another.inStock;
        this.price = another.price;
        this.imageUrl = another.imageUrl;
    }

    public Item(Long id, String name, String description, Integer inStock, Integer price, String imageUrl) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.inStock = inStock;
        this.price = price;
        this.imageUrl = imageUrl;
    }
}
