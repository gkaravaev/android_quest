package ru.nsk.nsu.playground.fitquest.presentation.shop;

import android.app.Activity;
import android.util.Log;
import android.widget.Toast;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import retrofit2.HttpException;
import ru.nsk.nsu.playground.fitquest.data.shop.IShopDataSource;
import ru.nsk.nsu.playground.fitquest.model.shop.Item;
import ru.nsk.nsu.playground.fitquest.presentation.leaderboard.LeaderboardListActivity;
import ru.nsk.nsu.playground.fitquest.presentation.stage.tasklist.TasksListActivity;
import ru.nsk.nsu.playground.fitquest.utils.UtilsFunctions;

import java.util.List;

public final class ShopPresenter implements ShopModuleContract.Presenter, ShopModuleContract.Router {

    private static final String LOG_TAG = ShopPresenter.class.getCanonicalName();

    private final ShopModuleContract.View view;

    IShopDataSource shopRepository;

    private Activity activity;
    private io.reactivex.rxjava3.disposables.Disposable toDestroy;

    public ShopPresenter(ShopModuleContract.View view, IShopDataSource dataSource) {
        this.view = view;
        this.shopRepository = dataSource;
        view.linkPresenter(this);
    }

    @Override
    public void loadShop() {
        Log.i(LOG_TAG, "Loading shop.");
        toDestroy = shopRepository
                .getShop()
                .subscribeOn(io.reactivex.rxjava3.schedulers.Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        stSuccess -> {
                            List<Item> shopItems = stSuccess;
                            Log.d(LOG_TAG, "Successfully obtained " + shopItems.size() + " leaders.");
                            view.showShop(shopItems);
                        },
                        stFail -> {
                            String errorMessage = stFail.getLocalizedMessage();
                            if (stFail instanceof HttpException) {
                                errorMessage = UtilsFunctions.httpExceptionToString((HttpException) stFail);
                            }
                            Log.e(LOG_TAG, errorMessage);
                        }
                );
    }

    @Override
    public void buyItem(String id) {
        shopRepository
                .buyItem(id)
                .subscribeOn(io.reactivex.rxjava3.schedulers.Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        stSuccess -> {
                            Toast toast = Toast.makeText(this.activity.getApplicationContext(), "Куплено успешно!", Toast.LENGTH_LONG);
                            toast.show();
                            view.onItemBought(id);
                        },
                        stFail -> {
                            String errorMessage = stFail.getLocalizedMessage();
                            if (stFail instanceof HttpException) {
                                errorMessage = UtilsFunctions.httpExceptionToString((HttpException) stFail);
                            }
                            Log.e(LOG_TAG, errorMessage);
                        }
                );
    }

    @Override
    public void onDestroy() {
        if (toDestroy != null && !toDestroy.isDisposed()) {
            toDestroy.dispose();
        }
    }

    @Override
    public void setActivity(final Activity activity) {
        this.activity = activity;
    }

    @Override
    public void goToTasksScreen() {
        UtilsFunctions.travelToActivity(this.activity, TasksListActivity.class);
    }

    @Override
    public void goToLeaderboardScreen() {
        UtilsFunctions.travelToActivity(this.activity, LeaderboardListActivity.class);
    }
}
