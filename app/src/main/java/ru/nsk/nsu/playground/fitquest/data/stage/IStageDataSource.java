package ru.nsk.nsu.playground.fitquest.data.stage;

import io.reactivex.rxjava3.core.Observable;
import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.Path;
import ru.nsk.nsu.playground.fitquest.model.common.ResponseResult;
import ru.nsk.nsu.playground.fitquest.model.stage.Answer;
import ru.nsk.nsu.playground.fitquest.model.stage.AnswerResult;
import ru.nsk.nsu.playground.fitquest.model.stage.Stage;
import ru.nsk.nsu.playground.fitquest.model.stage.TaskCreateRequest;
import ru.nsk.nsu.playground.fitquest.model.user.Credentials;
import ru.nsk.nsu.playground.fitquest.model.user.LoginResult;

public interface IStageDataSource {
    Observable<Stage> getStage();

    Observable<AnswerResult> answer(String id, Answer answer);

    Observable<ResponseResult> createTask(TaskCreateRequest request);
}
