package ru.nsk.nsu.playground.fitquest.presentation.register;

import android.app.Activity;
import android.util.Log;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.disposables.Disposable;
import retrofit2.HttpException;
import ru.nsk.nsu.playground.fitquest.data.user.IAuthDataSource;
import ru.nsk.nsu.playground.fitquest.model.user.RegistrationRequest;
import ru.nsk.nsu.playground.fitquest.presentation.login.LoginScreenActivity;
import ru.nsk.nsu.playground.fitquest.presentation.stage.tasklist.TasksListActivity;
import ru.nsk.nsu.playground.fitquest.utils.UtilsFunctions;

public class RegistrationPresenter implements RegistrationModuleContract.Presenter, RegistrationModuleContract.Router {

    private Activity activity;
    private RegistrationModuleContract.View view;
    private Disposable registerObservable;

    private IAuthDataSource authRepository;

    public RegistrationPresenter(RegistrationModuleContract.View view, IAuthDataSource authRepository) {
        this.authRepository = authRepository;
        this.view = view;
        view.linkPresenter(this);
    }

    @Override
    public void register(RegistrationRequest request) {
        registerObservable = authRepository
                .registration(request)
                .subscribeOn(io.reactivex.rxjava3.schedulers.Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        success -> {
                            view.onRegistrationSuccess();
                        },
                        error -> {
                            if (error instanceof HttpException){
                                String errorMessage = UtilsFunctions.httpExceptionToString((HttpException) error);
                                view.onRegistrationFail(errorMessage);
                            }
                            Log.i("REGISTRATION FAIL", error.getLocalizedMessage());
                        }
                );
    }

    @Override
    public void onDestroy() {
        if (registerObservable != null && !registerObservable.isDisposed()) {
            registerObservable.dispose();
        }
    }

    @Override
    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    @Override
    public void goToLogin() {
        UtilsFunctions.travelToActivity(this.activity, LoginScreenActivity.class);
    }

}
