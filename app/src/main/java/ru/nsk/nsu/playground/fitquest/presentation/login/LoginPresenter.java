package ru.nsk.nsu.playground.fitquest.presentation.login;

import android.app.Activity;
import android.util.Log;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import retrofit2.HttpException;
import ru.nsk.nsu.playground.fitquest.data.user.IAuthDataSource;
import ru.nsk.nsu.playground.fitquest.model.user.Credentials;
import ru.nsk.nsu.playground.fitquest.presentation.register.RegisterScreenActivity;
import ru.nsk.nsu.playground.fitquest.presentation.stage.tasklist.TasksListActivity;
import ru.nsk.nsu.playground.fitquest.utils.TokenInterceptor;
import ru.nsk.nsu.playground.fitquest.utils.UtilsFunctions;
import io.reactivex.rxjava3.disposables.Disposable;

public class LoginPresenter implements LoginModuleContract.Presenter, LoginModuleContract.Router {

    private Activity activity;
    private LoginModuleContract.View view;
    private Disposable loginObservable;

    private IAuthDataSource authRepository;
    private TokenInterceptor tokenInterceptor;

    public LoginPresenter(LoginModuleContract.View view, IAuthDataSource authRepository, TokenInterceptor tokenInterceptor) {
        this.authRepository = authRepository;
        this.view = view;
        this.tokenInterceptor = tokenInterceptor;
        view.linkPresenter(this);
    }

    @Override
    public void login(Credentials credentials) {
        loginObservable = authRepository
                .login(credentials)
                .subscribeOn(io.reactivex.rxjava3.schedulers.Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        success -> {
                            tokenInterceptor.setSessionToken(success.getToken());
                            Log.i("LOGIN SUCCESS", String.format("Token: %s", success.getToken()));
                            view.onLoginSuccess();
                        },
                        error -> {
                            if (error instanceof HttpException){
                                String errorMessage = UtilsFunctions.httpExceptionToString((HttpException) error);
                                view.onLoginFail(errorMessage);
                            }
                            Log.i("LOGIN FAIL", error.getLocalizedMessage());
                        }
                );
    }

    @Override
    public void onDestroy() {
        if (loginObservable != null && !loginObservable.isDisposed()) {
            loginObservable.dispose();
        }
    }

    @Override
    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    @Override
    public void goToTheTasks() {
        UtilsFunctions.travelToActivity(this.activity, TasksListActivity.class);
    }

    @Override
    public void goToTheRegistration() {
        UtilsFunctions.travelToActivity(this.activity, RegisterScreenActivity.class);
    }
}
