package ru.nsk.nsu.playground.fitquest.model.stage;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TaskCreateRequest {
    private String name;

    private String text;

    private String answer;

    private int points;

    public TaskCreateRequest(String name, String text, String answer, int points) {
        this.name = name;
        this.text = text;
        this.answer = answer;
        this.points = points;
    }
}
