package ru.nsk.nsu.playground.fitquest.presentation.shop;

import android.app.Activity;
import ru.nsk.nsu.playground.fitquest.model.leaderboard.LeaderboardItem;
import ru.nsk.nsu.playground.fitquest.model.shop.Item;
import ru.nsk.nsu.playground.fitquest.presentation.common.IBasePresenter;
import ru.nsk.nsu.playground.fitquest.presentation.common.IBaseView;

import java.util.List;

public interface ShopModuleContract {

    interface View extends IBaseView<Presenter> {
        void showShop(final List<Item> items);
        void onItemBought(String id);
    }

    interface Presenter extends IBasePresenter {
        void loadShop();
        void buyItem(String id);
        void onDestroy();
    }

    interface Router {
        void setActivity(final Activity activity);
        void goToTasksScreen();
        void goToLeaderboardScreen();
    }
}
