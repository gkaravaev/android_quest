package ru.nsk.nsu.playground.fitquest.data.shop;

import retrofit2.http.GET;
import io.reactivex.rxjava3.core.Observable;
import retrofit2.http.POST;
import retrofit2.http.Path;
import ru.nsk.nsu.playground.fitquest.model.common.ResponseResult;
import ru.nsk.nsu.playground.fitquest.model.shop.Item;

import java.util.List;

public interface ShopService {
    @GET("/api/shop/all")
    Observable<List<Item>> getShop();

    @POST("/api/shop/purchase/{id}")
    Observable<ResponseResult> buyItem(@Path("id") String id);
}
