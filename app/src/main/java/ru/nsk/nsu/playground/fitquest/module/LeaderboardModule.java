package ru.nsk.nsu.playground.fitquest.module;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import dagger.hilt.InstallIn;
import dagger.hilt.android.components.ActivityComponent;
import retrofit2.Retrofit;
import ru.nsk.nsu.playground.fitquest.data.leaderboard.ILeaderboardDataSource;
import ru.nsk.nsu.playground.fitquest.data.leaderboard.LeaderboardDataSourceImpl;
import ru.nsk.nsu.playground.fitquest.data.leaderboard.LeaderboardService;
import ru.nsk.nsu.playground.fitquest.data.stage.IStageDataSource;
import ru.nsk.nsu.playground.fitquest.data.stage.StageDataSourceImpl;
import ru.nsk.nsu.playground.fitquest.data.stage.StageService;


@Module
@InstallIn(ActivityComponent.class)
public abstract class LeaderboardModule {

    @Provides
    public static LeaderboardService provideLeaderboardService(Retrofit retrofit) {
        return retrofit.create(LeaderboardService.class);
    }

    @Binds
    public abstract ILeaderboardDataSource bindLeaderboardDataSource(LeaderboardDataSourceImpl leaderboardDataSource);

}

