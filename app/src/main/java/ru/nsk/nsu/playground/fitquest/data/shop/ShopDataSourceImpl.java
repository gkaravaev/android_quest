package ru.nsk.nsu.playground.fitquest.data.shop;

import io.reactivex.rxjava3.core.Observable;
import ru.nsk.nsu.playground.fitquest.model.common.ResponseResult;
import ru.nsk.nsu.playground.fitquest.model.shop.Item;

import javax.inject.Inject;
import java.util.List;

public class ShopDataSourceImpl implements IShopDataSource {

    private final ShopService shopService;

    @Inject
    public ShopDataSourceImpl(ShopService shopService) {
        this.shopService = shopService;
    }

    @Override
    public Observable<List<Item>> getShop() {
        return shopService.getShop();
    }

    @Override
    public Observable<ResponseResult> buyItem(String id) {
        return shopService.buyItem(id);
    }
}
