package ru.nsk.nsu.playground.fitquest.model.stage;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.Expose;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Task implements Parcelable {

    private Long id;

    private String created;

    private String updated;

    private Long stageId;

    private Boolean isHidden;

    private Boolean isCompleted;

    private Integer tries;

    private String name;

    private String text;

    private String imageUrl;

    private Integer points;

    public Task(Long stageId, Boolean isHidden, Boolean isCompleted, Integer tries, String name, String text, String imageUrl, Integer points) {
        this.stageId = stageId;
        this.isHidden = isHidden;
        this.isCompleted = isCompleted;
        this.tries = tries;
        this.name = name;
        this.text = text;
        this.imageUrl = imageUrl;
        this.points = points;
    }

    protected Task(Parcel in) {
        String[] stringFields = new String[5];
        boolean[] boolFields = new boolean[2];
        long[] longFields = new long[4];

        in.readStringArray(stringFields);
        in.readBooleanArray(boolFields);
        in.readLongArray(longFields);

        this.created = stringFields[0];
        this.imageUrl = stringFields[1];
        this.name = stringFields[2];
        this.text = stringFields[3];
        this.updated = stringFields[4];

        this.isCompleted = boolFields[0];
        this.isHidden = boolFields[1];

        this.id = longFields[0];
        this.points = Math.toIntExact(longFields[1]);
        this.stageId = longFields[2];
        this.tries = Math.toIntExact(longFields[3]);
    }

    public static final Creator<Task> CREATOR = new Creator<Task>() {
        @Override
        public Task createFromParcel(Parcel in) {
            return new Task(in);
        }

        @Override
        public Task[] newArray(int size) {
            return new Task[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringArray(new String[]{this.created, this.imageUrl, this.name, this.text, this.updated});
        dest.writeBooleanArray(new boolean[]{this.isCompleted, this.isHidden});
        dest.writeLongArray(new long[]{this.id, this.points, this.stageId, this.tries});
    }
}
