package ru.nsk.nsu.playground.fitquest.module;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import dagger.hilt.InstallIn;
import dagger.hilt.android.components.ActivityComponent;
import retrofit2.Retrofit;
import ru.nsk.nsu.playground.fitquest.data.shop.IShopDataSource;
import ru.nsk.nsu.playground.fitquest.data.shop.ShopDataSourceImpl;
import ru.nsk.nsu.playground.fitquest.data.shop.ShopService;
import ru.nsk.nsu.playground.fitquest.data.stage.IStageDataSource;
import ru.nsk.nsu.playground.fitquest.data.stage.StageDataSourceImpl;
import ru.nsk.nsu.playground.fitquest.data.stage.StageService;


@Module
@InstallIn(ActivityComponent.class)
public abstract class ShopModule {

    @Provides
    public static ShopService provideShopService(Retrofit retrofit) {
        return retrofit.create(ShopService.class);
    }

    @Binds
    public abstract IShopDataSource bindShopDataSource(ShopDataSourceImpl shopDataSource);

}

