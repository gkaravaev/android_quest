package ru.nsk.nsu.playground.fitquest.presentation.shop;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import ru.nsk.nsu.playground.fitquest.R;
import ru.nsk.nsu.playground.fitquest.model.leaderboard.LeaderboardItem;
import ru.nsk.nsu.playground.fitquest.model.shop.Item;

import java.util.List;

public final class ShopListAdapter extends RecyclerView.Adapter<ShopItemViewHolder> {

    private final ShopPresenter presenter;
    private List<Item> items;

    public ShopListAdapter(final ShopPresenter presenter, List<Item> list) {
        this.presenter = presenter;
        this.items = list;
    }

    void replaceData(final List<Item> items) {
        if (null != items) {
            this.items = items;
        }
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ShopItemViewHolder onCreateViewHolder(@NonNull final ViewGroup parent, final int viewType) {
        final Context context = parent.getContext();

        final View view = LayoutInflater
                .from(context)
                .inflate(R.layout.item_list_shop, parent, false);

        return new ShopItemViewHolder(view, presenter);
    }

    @Override
    public void onBindViewHolder(@NonNull final ShopItemViewHolder holder, final int position) {
        final Item item = items.get(position);
        holder.onBind(item);
    }

    @Override
    public int getItemCount() {
        return (null != items) ? items.size() : 0;
    }
}
