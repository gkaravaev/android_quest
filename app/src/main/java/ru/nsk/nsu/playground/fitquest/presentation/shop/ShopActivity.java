package ru.nsk.nsu.playground.fitquest.presentation.shop;

import android.os.Bundle;
import android.util.Log;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import dagger.hilt.android.AndroidEntryPoint;
import ru.nsk.nsu.playground.fitquest.R;
import ru.nsk.nsu.playground.fitquest.data.shop.IShopDataSource;
import ru.nsk.nsu.playground.fitquest.model.shop.Item;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@AndroidEntryPoint
public final class ShopActivity extends AppCompatActivity implements ShopModuleContract.View {

    private static final String LOG_TAG = ShopActivity.class.getSimpleName();

    private ShopModuleContract.Presenter presenter;

    private RecyclerView recyclerView;
    private BottomNavigationView bnv;

    private ShopListAdapter listAdapter;
    private List<Item> items;

    @Inject
    IShopDataSource shopDataSource;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop);

        setTitle("Магазин");

        ShopPresenter presenter = new ShopPresenter(this, shopDataSource);
        presenter.setActivity(this);

        bnv = findViewById(R.id.shop_list_bottom_navigation);

        bnv.setOnNavigationItemSelectedListener(item -> {
            switch (item.getItemId()) {
                case R.id.page_tasks:
                    presenter.goToTasksScreen();
                    return true;
                case R.id.page_shop:
                    return true;
                case R.id.page_leaderboard:
                    presenter.goToLeaderboardScreen();
                    return true;
            }
            return false;
        });

        this.listAdapter = new ShopListAdapter(presenter, new ArrayList<>());

        recyclerView = findViewById(R.id.recycler_view_shop);
        final RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(listAdapter);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(LOG_TAG, "onStart: ");
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(LOG_TAG, "onResume: ");
        presenter.loadShop();
    }

    @Override
    public void onDestroy() {
        presenter.onDestroy();
        Log.d(LOG_TAG, "onDestroy: ");
        presenter = null;
        super.onDestroy();
    }

    @Override
    public void linkPresenter(final ShopModuleContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void showShop(List<Item> items) {
        listAdapter.replaceData(items);
        this.items = items;
    }

    @Override
    public void onItemBought(String id) {
        this.items = items.stream()
                .map(item -> {
                    if (id.equals(item.getId().toString())) {
                        item.setInStock(item.getInStock() - 1);
                    }
                    return item;
                })
                .collect(Collectors.toList());
        listAdapter.replaceData(items);
    }
}
