package ru.nsk.nsu.playground.fitquest.module;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import dagger.hilt.InstallIn;
import dagger.hilt.android.components.ActivityComponent;
import retrofit2.Retrofit;
import ru.nsk.nsu.playground.fitquest.data.user.AuthDataSourceImpl;
import ru.nsk.nsu.playground.fitquest.data.user.AuthenticationService;
import ru.nsk.nsu.playground.fitquest.data.user.IAuthDataSource;


@Module
@InstallIn(ActivityComponent.class)
public abstract class AuthModule {

    @Provides
    public static AuthenticationService provideAuthenticationService(Retrofit retrofit) {
        return retrofit.create(AuthenticationService.class);
    }

    @Binds
    public abstract IAuthDataSource bindAuthDataSource(AuthDataSourceImpl authDataSource);
}

