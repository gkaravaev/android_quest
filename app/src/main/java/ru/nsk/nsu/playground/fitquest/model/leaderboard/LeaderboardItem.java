package ru.nsk.nsu.playground.fitquest.model.leaderboard;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LeaderboardItem {
    private String name;
    private int points;

    public LeaderboardItem(String name, int points) {
        this.name = name;
        this.points = points;
    }
}
