package ru.nsk.nsu.playground.fitquest.model.user;

import lombok.Getter;

@Getter
public class LoginResult {
    private String email;
    private String token;
}
