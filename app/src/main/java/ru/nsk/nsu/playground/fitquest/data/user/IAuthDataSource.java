package ru.nsk.nsu.playground.fitquest.data.user;

import io.reactivex.rxjava3.core.Observable;
import ru.nsk.nsu.playground.fitquest.model.common.ResponseResult;
import ru.nsk.nsu.playground.fitquest.model.user.Credentials;
import ru.nsk.nsu.playground.fitquest.model.user.LoginResult;
import ru.nsk.nsu.playground.fitquest.model.user.RegistrationRequest;

public interface IAuthDataSource {
    Observable<LoginResult> login(Credentials credentials);
    Observable<ResponseResult> registration(RegistrationRequest request);
}
