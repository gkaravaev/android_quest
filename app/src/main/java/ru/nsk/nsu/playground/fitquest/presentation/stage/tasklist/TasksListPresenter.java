package ru.nsk.nsu.playground.fitquest.presentation.stage.tasklist;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import retrofit2.HttpException;
import ru.nsk.nsu.playground.fitquest.data.stage.IStageDataSource;
import ru.nsk.nsu.playground.fitquest.model.stage.Task;
import ru.nsk.nsu.playground.fitquest.presentation.leaderboard.LeaderboardListActivity;
import ru.nsk.nsu.playground.fitquest.presentation.shop.ShopActivity;
import ru.nsk.nsu.playground.fitquest.presentation.stage.taskcreate.TaskCreateActivity;
import ru.nsk.nsu.playground.fitquest.presentation.stage.taskdetails.TaskDetailsActivity;
import ru.nsk.nsu.playground.fitquest.utils.UtilsFunctions;

import java.util.List;

public final class TasksListPresenter implements TasksListModuleContract.Presenter, TasksListModuleContract.Router {

    private static final String LOG_TAG = TasksListPresenter.class.getCanonicalName();

    private final TasksListModuleContract.View view;

    IStageDataSource stageRepository;

    private Activity activity;
    private io.reactivex.rxjava3.disposables.Disposable toDestroy;

    public TasksListPresenter(TasksListModuleContract.View view, IStageDataSource dataSource) {
        this.view = view;
        this.stageRepository = dataSource;
        view.linkPresenter(this);
    }

    @Override
    public void loadTasks() {
        Log.i(LOG_TAG, "Loading tasks.");
        toDestroy = stageRepository
                .getStage()
                .subscribeOn(io.reactivex.rxjava3.schedulers.Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        stSuccess -> {
                            List<Task> tasks = stSuccess.getTasks();
                            Log.d(LOG_TAG, "Successfully obtained " + tasks.size() + " tasks.");
                            view.showTasks(stSuccess.getTasks());
                        },
                        stFail -> {
                            String errorMessage = stFail.getLocalizedMessage();
                            if (stFail instanceof HttpException) {
                                errorMessage = UtilsFunctions.httpExceptionToString((HttpException) stFail);
                            }
                            Log.i("Stage FAIL", errorMessage);
                        }
                );
    }

    @Override
    public void onDestroy() {
        if (toDestroy != null && !toDestroy.isDisposed()){
            toDestroy.dispose();
        }
    }

    @Override
    public void setActivity(final Activity activity) {
        this.activity = activity;
    }

    @Override
    public void goToTheDetailsScreen(final Task task) {
        final Intent intent = TaskDetailsActivity.getIntent(activity, task);
        activity.startActivity(intent);
    }

    @Override
    public void goToTaskCreateScreen() {
        UtilsFunctions.travelToActivity(this.activity, TaskCreateActivity.class);
    }

    @Override
    public void goToLeaderboard() {
        UtilsFunctions.travelToActivity(this.activity, LeaderboardListActivity.class);
    }

    @Override
    public void goToShop() {
        UtilsFunctions.travelToActivity(this.activity, ShopActivity.class);
    }
}
