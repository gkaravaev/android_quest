package ru.nsk.nsu.playground.fitquest.presentation.leaderboard;

import android.app.Activity;
import ru.nsk.nsu.playground.fitquest.model.leaderboard.LeaderboardItem;
import ru.nsk.nsu.playground.fitquest.model.stage.Task;
import ru.nsk.nsu.playground.fitquest.presentation.common.IBasePresenter;
import ru.nsk.nsu.playground.fitquest.presentation.common.IBaseView;

import java.util.List;

public interface LeaderboardModuleContract {

    interface View extends IBaseView<Presenter> {
        void showLeaders(final List<LeaderboardItem> items);
    }

    interface Presenter extends IBasePresenter {
        void loadLeaders();
        void onDestroy();
    }

    interface Router {
        void setActivity(final Activity activity);
        void goToTasksScreen();
        void goToShopScreen();
    }
}
