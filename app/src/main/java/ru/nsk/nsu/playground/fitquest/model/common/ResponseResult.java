package ru.nsk.nsu.playground.fitquest.model.common;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ResponseResult {

    private String message;

    public ResponseResult(String message) {
        this.message = message;
    }
}
