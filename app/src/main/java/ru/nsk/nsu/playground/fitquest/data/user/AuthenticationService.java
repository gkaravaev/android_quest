package ru.nsk.nsu.playground.fitquest.data.user;

import io.reactivex.rxjava3.core.Observable;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import ru.nsk.nsu.playground.fitquest.model.common.ResponseResult;
import ru.nsk.nsu.playground.fitquest.model.user.Credentials;
import ru.nsk.nsu.playground.fitquest.model.user.LoginResult;
import ru.nsk.nsu.playground.fitquest.model.user.RegistrationRequest;
import ru.nsk.nsu.playground.fitquest.utils.TokenInterceptor;

public interface AuthenticationService {
    @POST("/api/login")
    @Headers(value = TokenInterceptor.NO_AUTH_HEADER)
    Observable<LoginResult> login(@Body Credentials credentials);

    @POST("/api/register")
    @Headers(value = TokenInterceptor.NO_AUTH_HEADER)
    Observable<ResponseResult> register(@Body RegistrationRequest request);
}
