package ru.nsk.nsu.playground.fitquest.application;

/**
 * В данном классе будут располагаться константы, которые мы можем использовать на уровне приложения.
 * Имеет смысл сюда выносить значения для сетевых запросов: base url сервера, endpoint'ы, порты и т.п.
 * Помимо этого сюда можно запихать ключи, по которым мы будем передавать extra-параметры в разные Activity.
 */
public final class Constants {

    public final class Extra {
        public static final String NOTE = "NoteExtraKey";
        public static final String TASK = "TaskExtraKey";
    }

    private Constants() {}
}
