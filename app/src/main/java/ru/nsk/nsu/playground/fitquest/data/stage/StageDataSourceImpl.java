package ru.nsk.nsu.playground.fitquest.data.stage;

import io.reactivex.rxjava3.core.Observable;
import ru.nsk.nsu.playground.fitquest.model.common.ResponseResult;
import ru.nsk.nsu.playground.fitquest.model.stage.Answer;
import ru.nsk.nsu.playground.fitquest.model.stage.AnswerResult;
import ru.nsk.nsu.playground.fitquest.model.stage.Stage;
import ru.nsk.nsu.playground.fitquest.model.stage.TaskCreateRequest;

import javax.inject.Inject;

public class StageDataSourceImpl implements IStageDataSource {

    private final StageService stageService;

    @Inject
    public StageDataSourceImpl(StageService stageService) {
        this.stageService = stageService;
    }

    @Override
    public Observable<Stage> getStage() {
        return stageService.getStage();
    }

    @Override
    public Observable<AnswerResult> answer(String id, Answer answer) {
        return stageService.answer(id, answer);
    }

    @Override
    public Observable<ResponseResult> createTask(TaskCreateRequest request) {
        return stageService.createTask(request);
    }
}
