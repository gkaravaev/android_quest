package ru.nsk.nsu.playground.fitquest.model.user;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RegistrationRequest {
    public RegistrationRequest(String email, String password, String firstName, String lastName) {
        this.email = email;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.contactInfo = "fill later";
        this.isInAcadem = false;
    }

    private String email;

    private String password;

    private String firstName;

    private String lastName;

    private String contactInfo;

    private Boolean isInAcadem;
}
