package ru.nsk.nsu.playground.fitquest.data.stage;

import io.reactivex.rxjava3.core.Observable;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import ru.nsk.nsu.playground.fitquest.model.common.ResponseResult;
import ru.nsk.nsu.playground.fitquest.model.stage.Answer;
import ru.nsk.nsu.playground.fitquest.model.stage.AnswerResult;
import ru.nsk.nsu.playground.fitquest.model.stage.Stage;
import ru.nsk.nsu.playground.fitquest.model.stage.TaskCreateRequest;

public interface StageService {
    @GET("/api/stage/current")
    Observable<Stage> getStage();

    @POST("/api/tasks/{id}/answer")
    Observable<AnswerResult> answer(@Path("id") String id, @Body Answer answer);

    @POST("/api/admin/config/createtask")
    Observable<ResponseResult> createTask(@Body TaskCreateRequest request);
}
