package ru.nsk.nsu.playground.fitquest.presentation.shop;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.recyclerview.widget.RecyclerView;
import ru.nsk.nsu.playground.fitquest.R;
import ru.nsk.nsu.playground.fitquest.model.leaderboard.LeaderboardItem;
import ru.nsk.nsu.playground.fitquest.model.shop.Item;

public final class ShopItemViewHolder extends RecyclerView.ViewHolder {

    private static final String LOG_TAG = ShopItemViewHolder.class.getCanonicalName();

    private TextView titleTextView;
    private TextView descTextView;
    private AppCompatImageButton buyButton;
    private ShopModuleContract.Presenter presenter;

    private Item item;

    public ShopItemViewHolder(@NonNull final View itemView, ShopModuleContract.Presenter presenter) {
        super(itemView);
        titleTextView = itemView.findViewById(R.id.shop_title_text_view);
        descTextView = itemView.findViewById(R.id.shop_desc_text_view);
        buyButton = itemView.findViewById(R.id.shop_button);
        this.presenter = presenter;

    }

    void onBind(final Item item) {
        this.item = item;
        titleTextView.setText(String.format("%s (Цена: %d, в наличии: %d)", item.getName(), item.getPrice(), item.getInStock()));
        descTextView.setText(item.getDescription());

        buyButton.setOnClickListener(view -> presenter.buyItem(item.getId().toString()));
    }
}