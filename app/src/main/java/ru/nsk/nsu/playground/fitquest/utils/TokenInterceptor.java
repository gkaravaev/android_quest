package ru.nsk.nsu.playground.fitquest.utils;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.io.IOException;

@Singleton
public class TokenInterceptor implements Interceptor {

    private static final String NO_AUTH_HEADER_KEY = "NoAuth";
    public static final String NO_AUTH_HEADER = "NoAuth: true";
    private String sessionToken;

    @Inject
    public TokenInterceptor() {
    }

    public void setSessionToken(String sessionToken) {
        this.sessionToken = sessionToken;
    }

    @Override public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();

        Request.Builder requestBuilder = request.newBuilder();

        if (request.header(NO_AUTH_HEADER_KEY) == null) {
            if (sessionToken == null) {
                throw new RuntimeException("Session token should be defined for auth apis");
            } else {
                requestBuilder.addHeader("Authorization", "Bearer " + sessionToken);
            }
        }

        return chain.proceed(requestBuilder.build());
    }
}
