package ru.nsk.nsu.playground.fitquest.presentation.login;

import android.app.Activity;
import ru.nsk.nsu.playground.fitquest.model.user.Credentials;
import ru.nsk.nsu.playground.fitquest.presentation.common.IBasePresenter;
import ru.nsk.nsu.playground.fitquest.presentation.common.IBaseView;

public interface LoginModuleContract {

    interface View extends IBaseView<LoginModuleContract.Presenter> {
        void onLoginSuccess();
        void onLoginFail(String message);
    }

    interface Presenter extends IBasePresenter {
        void login(Credentials credentials);
        void onDestroy();
    }

    interface Router {
        void setActivity(final Activity activity);
        void goToTheTasks();
        void goToTheRegistration();
    }
}
