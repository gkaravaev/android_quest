package ru.nsk.nsu.playground.fitquest.presentation.stage.tasklist;

import android.app.Activity;
import ru.nsk.nsu.playground.fitquest.model.stage.Task;
import ru.nsk.nsu.playground.fitquest.presentation.common.IBasePresenter;
import ru.nsk.nsu.playground.fitquest.presentation.common.IBaseView;

import java.util.List;

public interface TasksListModuleContract {

    interface View extends IBaseView<Presenter> {
        void showTasks(final List<Task> items);
    }

    interface Presenter extends IBasePresenter {
        void loadTasks();
        void onDestroy();
    }

    interface Router {
        void setActivity(final Activity activity);
        void goToTheDetailsScreen(final Task item);
        void goToTaskCreateScreen();
        void goToLeaderboard();
        void goToShop();
    }
}
