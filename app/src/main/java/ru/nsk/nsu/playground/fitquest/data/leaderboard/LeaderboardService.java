package ru.nsk.nsu.playground.fitquest.data.leaderboard;

import io.reactivex.rxjava3.core.Observable;
import retrofit2.http.GET;
import ru.nsk.nsu.playground.fitquest.model.leaderboard.LeaderboardItem;

public interface LeaderboardService {
    @GET("/api/leaderboard/get")
    Observable<LeaderboardItem[]> getLeaderboard();
}
