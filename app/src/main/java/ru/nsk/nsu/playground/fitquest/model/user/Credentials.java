package ru.nsk.nsu.playground.fitquest.model.user;


public class Credentials {
    public Credentials(String email, String password) {
        this.email = email;
        this.password = password;
    }

    private String email;
    private String password;
}
