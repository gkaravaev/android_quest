package ru.nsk.nsu.playground.fitquest.presentation.leaderboard;

import android.util.Log;
import android.view.View;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import ru.nsk.nsu.playground.fitquest.R;
import ru.nsk.nsu.playground.fitquest.model.leaderboard.LeaderboardItem;
import ru.nsk.nsu.playground.fitquest.model.stage.Task;

public final class LeaderboardItemViewHolder extends RecyclerView.ViewHolder {

    private static final String LOG_TAG = LeaderboardItemViewHolder.class.getCanonicalName();

    private TextView titleTextView;

    private LeaderboardItem item;

    public LeaderboardItemViewHolder(@NonNull final View itemView) {
        super(itemView);

        titleTextView = itemView.findViewById(R.id.leaderboard_title_text_view);
    }

    void onBind(final LeaderboardItem item, int position) {
        this.item = item;
        titleTextView.setText(String.format("%d) %s (%d очков)", position + 1, item.getName(), item.getPoints()));
    }
}